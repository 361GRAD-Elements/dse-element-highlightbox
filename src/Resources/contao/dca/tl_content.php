<?php

/**
 * 361GRAD Element Highlightbox
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_highlightbox'] =
    '{type_legend},type,headline,dse_secondline,dse_subheadline;' .
    '{highlightbox_legend},dse_text,dse_ctaHref,dse_ctaTitle,dse_customName,dse_customLocation,dse_bgcolor;' .
    '{invisible_legend:hide},invisible,start,stop';

// Element fields
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondline'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondline'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'maxlength' => 200,
        'tl_class'  => 'w50'
    ],
    'sql'       => 'varchar(255) NOT NULL'
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_subheadline'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_subheadline'],
    'search'    => true,
    'inputType' => 'inputUnit',
    'options'   => [
        'h2',
        'h3',
        'h4',
        'h5',
        'h6'
    ],
    'eval'      => [
        'maxlength' => 200,
        'tl_class'  => 'w50'
    ],
    'sql'       => 'varchar(255) NOT NULL'
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_text']        = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_text'],
    'inputType' => 'textarea',
    'eval'      => [
        'mandatory' => true,
        'tl_class'  => 'clr',
        'rte'       => 'tinyMCE',
    ],
    'sql'       => 'text NULL'
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_ctaHref'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_ctaHref'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'long clr wizard'
    ],
    'wizard'    => [
        [
            'tl_content',
            'pagePicker'
        ]
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_ctaTitle'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_ctaTitle'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50 ',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_customName'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_customName'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50 ',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];


$GLOBALS['TL_DCA']['tl_content']['fields']['dse_customLocation'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_customLocation'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50 ',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_bgcolor'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_bgcolor'],
    'default'   => 'none',
    'inputType' => 'radio',
    'options'   => [
        'none',
        // #373F43 anthrazit
        'anthracite',
        // #D3D3D3 hellgrau
        'lightgrey',
        // #FFFFFF weiß
        'white',
        // #FF4D00 orange
        'orange',
    ],
    'reference' => [
        'none'       => Dse\ElementsBundle\ElementHighlightbox\Element\ContentDseHighlightbox::refColor(
            '#FFFFFF',
            '#000',
            'Keine'
        ),
        'anthracite' => Dse\ElementsBundle\ElementHighlightbox\Element\ContentDseHighlightbox::refColor(
            '#373F43',
            '#FFF',
            'Anthrazit'
        ),
        'lightgrey'  => Dse\ElementsBundle\ElementHighlightbox\Element\ContentDseHighlightbox::refColor(
            '#D3D3D3',
            '#000',
            'Hellgrau'
        ),
        'white'      => Dse\ElementsBundle\ElementHighlightbox\Element\ContentDseHighlightbox::refColor(
            '#FFFFFF',
            '#000',
            'Weiß'
        ),
        'orange'     => Dse\ElementsBundle\ElementHighlightbox\Element\ContentDseHighlightbox::refColor(
            '#FF4D00',
            '#000',
            'Orange'
        ),
    ],
    'eval' => [
        'tl_class' => 'clr',
    ],
    'sql'       => "varchar(32) NOT NULL default ''"
];
