<?php

/**
 * 361GRAD Element Highlightbox
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']     = 'DSE-Elemente';
$GLOBALS['TL_LANG']['CTE']['dse_highlightbox'] = ['Highlightbox', 'Highlightbox'];

$GLOBALS['TL_LANG']['tl_content']['highlightbox_legend'] = 'Highlightbox';

$GLOBALS['TL_LANG']['tl_content']['secondline']  =
    ['Überschrift (Zeile 2)', 'Fügt einer Überschrift eine zweite Zeile hinzu'];
$GLOBALS['TL_LANG']['tl_content']['subheadline'] =
    ['Unterüberschrift', 'Fügt eine Subheadline hinzu'];

$GLOBALS['TL_LANG']['tl_content']['dse_text']     = ['Text', 'Hier können sie den Haupttext eingeben.'];
$GLOBALS['TL_LANG']['tl_content']['dse_ctaHref']  = [
    'CTA Link',
    'Hier definieren Sie den internen/externen Link zu einer Seite.'
];
$GLOBALS['TL_LANG']['tl_content']['dse_ctaTitle'] = [
    'CTA Titel',
    'Hier geben sie den CTA-Button Title an.'
];
$GLOBALS['TL_LANG']['tl_content']['dse_customName'] = [
    'Name',
    'Name des Textschreibers.'
];
$GLOBALS['TL_LANG']['tl_content']['dse_customLocation'] = [
    'Herkunft',
    'Ort des Textschreibers.'
];
$GLOBALS['TL_LANG']['tl_content']['dse_bgcolor']  = [
    'Hintergrundfarbe',
    'Gibt die Hintergrundfarbe der Highlightbox an.'
];
