<?php

/**
 * 361GRAD Element Highlightbox
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

namespace Dse\ElementsBundle\ElementHighlightbox\Element;

use Contao\BackendTemplate;
use Contao\ContentElement;
use Contao\StringUtil;
use Patchwork\Utf8;

/**
 * Class ContentDseHighlightbox
 *
 * @package Dse\ElementsBundle\Elements
 */
class ContentDseHighlightbox extends ContentElement
{
    /**
     * Template Name.
     *
     * @var string
     */
    protected $strTemplate = 'ce_dse_highlightbox';

    /**
     * Generate the module
     *
     * @return void
     */
    protected function compile()
    {
        $arrSubheadline              = StringUtil::deserialize($this->dse_subheadline);
        $this->Template->subheadline = is_array($arrSubheadline) ? $arrSubheadline['value'] : $arrSubheadline;
        $this->Template->shl         = is_array($arrSubheadline) ? $arrSubheadline['unit'] : 'h2';
    }

    /**
     * Creates special label for checkboxes with a colored style
     *
     * @param string $bgc   The Background Color.
     * @param string $fgc   The Foreground Color.
     * @param string $title The Label Title.
     *
     * @return string
     */
    public static function refColor($bgc, $fgc, $title)
    {
        $style = 'display:inline-block;width:100px;text-align:center;';
        return '<span style="background-color:' . $bgc . ';color:' . $fgc . ';' . $style . '">' . $title . '</span>';
    }
}
