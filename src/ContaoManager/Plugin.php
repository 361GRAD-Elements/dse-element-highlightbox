<?php

namespace Dse\ElementsBundle\ElementHighlightbox\ContaoManager;

use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Dse\ElementsBundle\ElementHighlightbox;
use Contao\CoreBundle\ContaoCoreBundle;

class Plugin implements BundlePluginInterface
{
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(ElementHighlightbox\DseElementHighlightbox::class)
                ->setLoadAfter([ContaoCoreBundle::class])
        ];
    }
}
